import React,{Component} from 'react';
import NavBar from "../../containers/Navbar/NavBar";

class Layout extends Component{
  render(){
      let navItems=['Home', 'About', 'Search','Settings'];
      return(
        <section>
            <NavBar navItems={navItems}/>
        </section>
      )
  }
}

export default Layout;