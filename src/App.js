import React from 'react';
import logo from './logo.svg';
import './App.css';
import Layout from './views/Layout/Layout'
import Home from './views/Home'
import Search from './views/Search'
function App() {
  return (

    <div className="App">
      <Layout/>
      <Home/>
      <Search/>
    </div>
  );
}

export default App;
